<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;
use App\Models\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'nama_lengkap'=>'superadmin',
            'username'=>'superadmin',
            'password'=>bcrypt('password'),
            'no_hp'=>'6289988766789',
            'email'=>'admin@admin.com',
            'status'=>'1',
            'photo' => 'user.png',
            'role' => 'Superadmin',
        ]);
    // User::factory(2)->create();
    }
}
