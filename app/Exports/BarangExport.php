<?php
namespace App\Exports;

use App\Models\Barang;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\FromCollection;
use Carbon\Carbon;

class BarangExport implements FromView
{
    protected $startDate;
    protected $endDate;

    public function __construct($startDate = null, $endDate = null)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    public function view(): View
    {
        if ($this->startDate && $this->endDate) {
            $startDate = Carbon::parse($this->startDate)->startOfDay();
            $endDate = Carbon::parse($this->endDate)->endOfDay();
        
            $barang = Barang::whereBetween('tanggal_terima', [$startDate, $endDate])->get();
        } else {
            $barang = Barang::all();
        }
    
        return view('barang.exportexcel', [
            'barang' => $barang
        ]);
    }
    
}
