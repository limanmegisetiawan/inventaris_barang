<?php
namespace App\Exports;

use App\Models\Kerusakan;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithHeadings;

class KerusakanExport implements FromView, WithHeadings
{
    protected $startDate;
    protected $endDate;

    public function __construct($startDate = null, $endDate = null)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    public function view(): View
    {
        $kerusakanQuery = Kerusakan::with('barang');
    
        if ($this->startDate && $this->endDate) {
            $startDate = $this->startDate;
            $endDate = $this->endDate;
    
            if ($startDate === $endDate) {
                $kerusakanQuery->whereDate('updated_at', $startDate);
            } else {
                $kerusakanQuery->whereBetween('updated_at', [$startDate, $endDate]);
            }
        }
    
        return view('kerusakan.exportexcel', [
            'kerusakan' => $kerusakanQuery->get(),
        ]);
    }
    

    public function headings(): array
    {
        return [
            'nama_pemilik',
            'nama_barang',
            'serial_number',
            'kode_barang',
            'jumlah_rusak',
            'satuan',
        ];
    }
}
