<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, $role)
    {
         // Check if the user has the specified role
        if (auth()->check() && auth()->user()->role !== $role) {
            // Redirect or show an error message
            return redirect('/')->with('error', 'Anda tidak memiliki izin untuk mengakses halaman ini');
        }

        return $next($request);
    }
}
