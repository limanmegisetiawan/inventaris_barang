<?php

namespace App\Http\Controllers;


use App\Models\Barang;
use App\Models\Kerusakan;
use Illuminate\Http\Request;
use App\Exports\BarangExport;
use App\Imports\BarangImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;

class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $totalBarang =Barang::count();
        $totalKerusakan = Kerusakan::where('status', '<>', 'Ditutup')->count();

        $barang = Barang::latest();
        if(request('search')){
            $barang->where('nama_pemilik','like','%'. request('search').'%')
            ->orWhere('nama_barang','like','%'. request('search').'%')
            ->orWhere('serial_number','like','%'. request('search').'%')
            ->orWhere('lokasi_barang','like','%'. request('search').'%');
        }
        $barang= $barang->simplePaginate(10);
        return view('barang.list_barang',compact(['barang','totalBarang', 'totalKerusakan']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $barang = barang:: all();

    return view ('barang/create_barang', compact(['barang']));
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ValidatedData= $request->validate([
            'nama_pemilik' =>'required|max:255',
            'nama_barang' =>'required|max:255',
            'serial_number' =>['required','min:3','max:255','unique:data_barang'],
            'kode_barang' =>['required','min:3','max:255','unique:data_barang'],
            'tanggal_terima' =>'required|max:255',
            'satuan' =>'required|max:255',
            'lokasi_barang' =>'required|max:255',
            'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048'

        ]);
        
        $data= Barang::create($ValidatedData);
        if($request->hasFile('image')){
            $request->file('image')->move('storage/',$request->file('image')->getClientOriginalName());
            $data->image=$request->file('image')->getClientOriginalName();
            $data->save();
        }
        

        return redirect('/barang')->with('success', 'Data Berhasil Ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $barang = Barang::find($id);
        return view('barang.lihat_barang',compact(['barang']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $barang = Barang::find($id);
        return view('barang.edit_barang',compact(['barang']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'nama_pemilik' =>'required|max:255',
            'nama_barang' =>'required|max:255',
            'serial_number' => 'required|min:3|max:25|unique:data_barang,serial_number,'.$id,
            'kode_barang' => 'required|min:3|max:25|unique:data_barang,kode_barang,'.$id,
            'tanggal_terima' =>'required|max:255',
            'satuan' =>'required|max:255',
            'lokasi_barang' =>'required|max:255',
            'image' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        ]);
        
        $barang = Barang::find($id);
    if ($request->hasFile('image')) {
        $destination = 'storage/' . $barang->image;
        if (Storage::exists($destination)) {
            Storage::delete($destination);
        }
    }

    $barang->update($request->except(['_token', 'submit', 'image']));

    if ($request->hasFile('image')) {
        $file = $request->file('image');
        $extension = $file->getClientOriginalExtension();
        $filename = time() . '.' . $extension;
        $file->move('storage/', $filename);
        $barang->image = $filename;
        $barang->save();
    }
        return redirect('/barang')->with('success', 'Data Berhasil Diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $barang = Barang::find($id);
        $barang->delete();
        return redirect('/barang');
    }

   
    public function exportexcel(Request $request)
    {
        if ($request->has('start_date') && $request->has('end_date')) {
            $request->validate([
                'start_date' => 'required|date',
                'end_date' => 'required|date|after_or_equal:start_date',
            ]);

            $startDate = $request->input('start_date');
            $endDate = $request->input('end_date');

            $fileName = 'barang-export-' . $startDate . '-to-' . $endDate . '.xlsx';
        } else {
            $fileName = 'barang-export-all-' . now()->format('Ymd') . '.xlsx';
        }

        return Excel::download(new BarangExport($startDate ?? null, $endDate ?? null), $fileName);
    }

    public function importexcel(Request $request)
    {
        $request->validate([
            'file'=>'required|mimes:xlsx'
        ]);
        try {
        Excel::import(new BarangImport, $request->file('file'));
        return redirect('/barang')->with('succes','Data Berhasil di Import');

        } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            $failures = $e->failures();
        return redirect('/barang')->with('import_errors',$failures);
            
        }
    
    }
}
