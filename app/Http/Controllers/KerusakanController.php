<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use App\Models\Kerusakan;
use App\Models\Activity;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\KerusakanExport;


class KerusakanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $totalBarang = Barang::count();
        $totalKerusakan = Kerusakan::where('status', '<>', 'Ditutup')->count();
        $totalService = Kerusakan::where('status', 'Service')->count();
        $totalSelesai = Kerusakan::where('status', 'Selesai')->count();
        $totalDiserahkan = Kerusakan::where('status', 'Diserahkan')->count();
    
        $activities = Activity::all(); 
        $barang = Barang::all();
        $kerusakan = Kerusakan::latest();

        $activities = Activity::orderBy('created_at', 'desc')->get();
        $searchBarang = null;

        $keyword = $request->input('search');

        $keyword = $request->input('search');

        $kerusakan = Kerusakan::with('barang')
            ->whereHas('barang', function ($query) use ($keyword) {
                $query->where('nama_barang', 'LIKE', '%' . $keyword . '%')
                    ->orwhere('nama_pemilik', 'LIKE', '%' . $keyword . '%');
            })
            ->simplePaginate(10);
    
        return view('kerusakan.list_kerusakan', compact('totalDiserahkan','totalSelesai','totalService','barang','searchBarang','activities','kerusakan', 'totalBarang', 'totalKerusakan'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kerusakan = kerusakan:: all();
        $barang = Barang:: all();
        $searchBarang = null;
        if(request('search')){
            $searchBarang = $barang->where('nama_barang', request('search'))->first();
        }
        return view ('kerusakan.create_kerusakan', compact(['kerusakan', 'barang', 'searchBarang']));
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
 
public function store(Request $request)
{
    $validatedData = $request->validate([
        'barang_id' => 'required',
        'kerusakan_barang' => 'required',
        'nama_penerima' => 'required',
    ]);

    $kerusakan = new Kerusakan();

    $kerusakan->barang_id = $request->input('barang_id');
    $kerusakan->kerusakan_barang = $request->input('kerusakan_barang');
    $kerusakan->status = 'Diterima';
    $kerusakan->jumlah_rusak = 1;
    $kerusakan->nama_penerima = $request->input('nama_penerima');
    $kerusakan->catatan_service = $request->input('catatan_service');
    $kerusakan->catatan_selesai = $request->input('catatan_selesai');
    $kerusakan->catatan_serahkan = $request->input('catatan_serahkan');
    $kerusakan->nama_penyervice = $request->input('nama_penyervice');
    $kerusakan->penerima_barang = $request->input('penerima_barang');
    $kerusakan->save();

    $dataBarang = Barang::find($kerusakan->barang_id);

    if ($dataBarang) {
        $newJumlah = max(0, $dataBarang->jumlah - $kerusakan->jumlah_rusak);
        $dataBarang->jumlah = $newJumlah;
        $dataBarang->save();
    }

    return redirect('/kerusakan')->with('success', 'Data Service Berhasil Ditambah');
}


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kerusakan = Kerusakan::find($id);

        return view('kerusakan.lihat_kerusakan',compact(['kerusakan']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kerusakan = Kerusakan::find($id);
        return view('kerusakan.edit_kerusakan',compact(['kerusakan']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function update(Request $request, $id)
{
    $kerusakan = Kerusakan::find($id);
    
    $statusAwal = $kerusakan->status;
    $kerusakan->update($request->except(['_token','submit']));
    
    // Update status if certain conditions are met
    if (($statusAwal === 'Selesai' && $request->status === 'Service') || $statusAwal === 'Diserahkan') {
        $kerusakan->status = $request->status;

        // Check if the status is 'ditutup'
        if ($request->status === 'ditutup') {
            // Retrieve the related data from jumlah_rusak
            $jumlahRusak = $kerusakan->jumlah_rusak;

            // Update the corresponding data in table jumlah
            // Assuming there is a relationship between Kerusakan and Jumlah models
            $kerusakan->jumlah()->increment('jumlah', $jumlahRusak);
        }
    }

    $kerusakan->save();

    return redirect('/kerusakan')->with('success', 'Data Service Berhasil Diupdate');
}

    

    public function showActivityLog(Kerusakan $kerusakan , Barang $id)
    {
        
        $activities = Activity::where('subject_type', Kerusakan::class)
            ->where('subject_id', $kerusakan->id)
            ->orderBy('created_at', 'desc')
            ->get();
            
            $dataBarang = [];
            
            foreach ($activities as $a) {
                $barangId = json_decode($a->properties)->attributes->barang_id;
            $barang = Barang::find($barangId);

            if ($barang) {
                $dataBarang[] = $barang;
            }
        }
        dd($activities);

        $lastActivity = $activities->first();
        $activityId = $lastActivity ? $lastActivity->id : null;
        
        return view('kerusakan.list_kerusakan', compact('barang','kerusakan', 'activities','activityId'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kerusakan = Kerusakan::find($id);
        $kerusakan->delete();
        return redirect('/kerusakan');
    }

    public function exportexcel(Request $request)
    {
        if ($request->has('start_date') && $request->has('end_date')) {
            $request->validate([
                'start_date' => 'required|date',
                'end_date' => 'required|date|after_or_equal:start_date',
            ]);

            $startDate = $request->input('start_date');
            $endDate = $request->input('end_date');

            $fileName = 'kerusakan-export-' . $startDate . '-to-' . $endDate . '.xlsx';
        } else {
            $fileName = 'kerusakan-export-all-' . now()->format('Ymd') . '.xlsx';
        }

        return Excel::download(new KerusakanExport($startDate ?? null, $endDate ?? null), $fileName);
    }

    public function filter(Request $request)
    {
        $status = $request->input('status');
        $barang = Barang::all(); 
        $totalKerusakan = Kerusakan::where('status', '<>', 'Ditutup')->count();


        $totalService = Kerusakan::where('status', 'Service')->count();
        $totalSelesai = Kerusakan::where('status', 'Selesai')->count();
        $totalDiserahkan = Kerusakan::where('status', 'Diserahkan')->count();

        $query = Kerusakan::query();
        
        if ($status === 'Diterima') {
            $query->where('status', 'Diterima');
        } elseif ($status === 'Service') {
            $query->where('status', 'Service');
        } elseif ($status === 'Selesai') {
            $query->where('status', 'Selesai');
        } elseif ($status === 'Selesai') {
            $query->where('status', 'Selesai');
        } elseif ($status === 'Diserahkan') {
            $query->where('status', 'Diserahkan');
        }elseif ($status === 'Ditutup') {
            $query->where('status', 'Ditutup');
        }
        
        $kerusakan = $query->paginate(10);
        $activities = Activity::orderBy('created_at', 'desc')->get();
        return view('kerusakan.list_kerusakan', compact('totalKerusakan','totalDiserahkan','totalSelesai','totalService','activities','kerusakan','barang'));
    }
}
