@extends('layouts.main')
@section('content')
<main id="main" class="main">
  <section class="section">
    <div class="row">
        <div class="card text-white bg-flat-color-1">
          <div class="text-start mt-4 mb-4 fw-bold">
            <i class="fa-solid fa-truck text-purple"></i>&nbsp;
            <span class="fw-bold text-muted mr-4">Data Barang</span>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-sm-6 col-lg-6">
                <div class="card text-white bg-flat-color-1">
                  <div class="card-body pb-0">
                    <div class="row">
                      <div class="col-lg-2">
                        <div class="text-black fw-bold d-flex align-items-center" style="background-color: #EEF4FF; padding: 5px; margin-bottom: 5px; width: fit-content;">
                          <i class="bi bi-box" style="font-size: 2.5rem; margin-right: 8px;"></i>
                        </div>
                      </div>
                      <div class="col-lg-8">
                        <div class="row">
                          <div class="col">
                            <div class="text-black fw-bold">
                              <span style="font-size: 20px;  margin-left: 8px;">Total barang secara keseluruhan</span>
                            </div>
                            <div class="text-black fw-bold">
                              <span style="font-size: 20px;  margin-left: 8px;"> {{ $totalBarang }}</span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-6 col-lg-6">
                <div class="card text-white bg-flat-color-1">
                  <div class="card-body pb-0">
                    <div class="row">
                      <div class="col-lg-2">
                        <div class="text-black fw-bold d-flex align-items-center" style="background-color: #EEF4FF; padding: 5px; margin-bottom: 5px; width: fit-content;">
                          <i class="bi bi-exclamation-triangle" style="font-size: 2.5rem; margin-right: 8px;"></i>
                        </div>
                      </div>
                      <div class="col-lg-8">
                        <div class="row">
                          <div class="col">
                            <div class="text-black fw-bold">
                              <span style="font-size: 20px;  margin-left: 8px;">Total kerusakan barang</span>
                            </div>
                            <div class="text-black fw-bold">
                              <span style="font-size: 20px;  margin-left: 8px;">{{ $totalKerusakan }}</span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="container">
            <div class="row g-2">
              <div class="col-8">
                <div class="border rounded bg-light">
                  <form action="/barang" class="search-form d-flex align-items-center">
                    <div class="input-group">
                      <input type="text" class="form-control" placeholder="Cari Nama Pemilik / Nama Barang / Serial Number / Lokasi Barang" name="search" value="{{ request('search') }}">
                      <button type="submit" class="btn text-start" style="border: none !important;">
                        <i class="bi bi-search"></i>
                      </button>
                    </div>
                  </form>
                </div>
              </div>
              <div class="col-1 col-sm-auto">
                <div class="border rounded bg-light">
                  <button type="button" class="btn btn-primary w-100 " data-bs-toggle="modal" data-bs-target="#importBarang">
                    Import
                  </button>
                </div>
              </div>
              <div class="col-1 col-sm-auto">
                <div class="border rounded bg-light">
                  <button type="button" class="btn btn-success w-100 " data-bs-toggle="modal" data-bs-target="#exportBarang">
                    Export
                  </button>
                </div>
              </div>
              <div class="col-1 col-sm-auto">
                <div class="border rounded bg-light">
                  <button type="button" class="btn btn-purple w-100" data-bs-toggle="modal" data-bs-target="#createBarang">
                    Tambah Barang
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div class="container mt-4">
            <div class="row">
              @if (session()->has('success'))
              <br>
                <div class="alert alert-success" role="alert">
                  {{ session('success') }}
                </div>
              <br>
              @endif
              @if ($errors->any())
              <br>
                <div class="alert alert-danger" role="alert">
                  @foreach ($errors->all() as $error)
                  {{ $error }}
                  @endforeach
                </div>
              <br>
              @endif
              @if (Session::has('import_errors'))
              <br>
                <div class="alert alert-danger" role="alert">
                  @foreach (Session::get('import_errors') as $failure)
                  {{ $failure->errors()[0] }} at line no-{{ $failure->row() }}
                  @endforeach
                </div>
              <br>
              @endif
              <div class="table-responsive">
                <table class="table table-hover table-sm align-middle nowrap">
                  <thead class="table">
                    <tr>
                      <th class="text-center" style="background-color: #355B74; color: #fff;">No</th>
                      <th class="text-center" style="background-color: #355B74; color: #fff;">Tanggal</th>
                      <th class="text-center" style="background-color: #355B74; color: #fff;">Nama Penanggung jawab</th>
                      <th class="text-center" style="background-color: #355B74; color: #fff;">Nama Barang</th>
                      <th class="text-center" style="background-color: #355B74; color: #fff;">Serial Number</th>
                      <th class="text-center" style="background-color: #355B74; color: #fff;">Gudang</th>
                      <th class="text-center col-2" style="background-color: #355B74; color: #fff;">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($barang as $i => $b)
                      <tr>
                        <td class="text-center">{{ $i + 1 }}</td>
                        {{-- <td class="text-center">{{ $b->tanggal_terima->format('d F Y') }}</td> --}}
                        <td class="text-center">{{ \Carbon\Carbon::parse($b->tanggal_terima)->format('d F Y') }}</td>

                        <td class="text-center">{{ $b->nama_pemilik }}</td>
                        <td class="text-center">{{ $b->nama_barang }}</td>
                        <td class="text-center">{{ $b->serial_number }}</td>
                        <td class="text-center">{{ $b->lokasi_barang }}</td>
                        <td>
                          <div class="text-center">
                            <form action="{{ url('barang/'.$b->id) }}" method="POST">
                              <button type="button" class="btn btn-sm btn-purple" data-bs-toggle="modal" data-bs-target="#lihatBarang{{ $b->id }}">
                                <i class=" bi bi-eye"></i>
                              </button>
                              
                              <button type="button" class="btn btn-sm btn-warning" data-bs-toggle="modal" data-bs-target="#editBarangs{{ $b->id }}">
                                <i class=" bi bi-pencil-square"></i>
                              </button>
                              @csrf 
                                <button class="btn btn-sm btn-danger" type="button" onclick="confirmDelete('{{ $b->id }}', '{{ $b->nama_barang }}')">
                                  <i class="bi bi-trash"></i>
                                </button>
                              <input type="hidden" name="_method" value="DELETE">
                            </form>
                          </div>
                          <div class="modal fade" id="editBarangs{{ $b->id }}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="editBarangs{{ $b->id }}Label" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="editBarangs{{ $b->id }}Label">Edit Barang</h5>
                                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                  @include('barang.edit_barang')
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal fade" id="lihatBarang{{ $b->id }}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticLabel{{ $b->id }}" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title">Detail Barang</h5>
                                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                  @include('barang.lihat_barang')
                                </div>
                              </div>
                            </div>
                          </div>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
                {{ $barang->links() }}
              </div>
            </div>
          </div>
        </div>
    </div>
  </section>
  <div class="modal fade" id="createBarang" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Tambah Barang</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          @include('barang.create_barang')
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="exportBarang" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Export Data Barang</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="border rounded bg-light">
          <form action="{{ route('barang.exportData') }}" method="POST">
            @csrf
            <div class="modal-body">
              <div class="form-group">
                <label class="text-start mt-2 mb-2 fw-bold">Export semua data barang dengan cara klik export semua data dibawah ini</label>
                <a href="{{ route('barang.exportDataAll') }}" class="btn btn-purple w-100" style="margin-bottom: 20px;">Export Semua Data</a>
                <div class="row">
                <label class="text-start mt-2 mb-2 fw-bold">Export data bedasarkan input tanggal yang anda pilih</label>
                <label class="text-start mt-2 mb-2 fw-bold">Sebelum export pastikan tanggal export dibawah ini terlebih dahulu</label>
                  <div class="col-6">
                    <div class="form-group">
                      <label for="start_date">Tanggal Awal</label>
                      <input type="date" id="start_date" name="start_date" class="form-control" required>
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="form-group">
                      <label for="end_date">Tanggal Akhir</label>
                      <input type="date" id="end_date" name="end_date" class="form-control" required>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <div class="col-12">
                <div class="row">
                  <div class="col-6"><button type="button" class="btn btn-outline-purple w-100" data-bs-dismiss="modal">Batal</button></div>
                  <div class="col-6"><button type="submit" class="btn btn-purple w-100">Export</button></div>
                </div>
              </div>
            </div>
          </form>
      </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="importBarang" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Import Data Barang</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <form action="/barang/importexcel" method="POST" enctype="multipart/form-data">
          @csrf
          <div class="modal-body">
            <div class="form-group">
              <label class="text-start mt-2 mb-2 fw-bold">Upload template excel pada form dibawah ini</label>
              <label class="text-start mt-2 mb-2 fw-bold">Sebelum import pastikan download template import dibawah ini terlebih dahulu</label>
              <a href="{{asset('Import/ImportBarang.xlsx') }}" class="btn btn-purple w-100" style="margin-bottom: 20px;">Download Template Import</a>
              <div class="row">
                <div style="margin: auto; width: 100%; text-align: center;">
                  <div style="background-color: #f5e1f7; border-radius: 10px; border: 3px dashed rgba(226, 10, 247, 0.65);">
                    <div style="background-color: rgb(245, 225, 247); border-radius: 10px; padding: 10px;">
                      <div class="drag-file" id="dragFileArea2">
                        <img class="navbar-brand" src="/images/frame.png" alt="Logo"><br>
                        <input type="file" name="file" class="form-control-file" id="file3" accept="file/*" required value="{{ old('file') }}" style="visibility: hidden"><br>
                        <label class="drag-file-text" for="file3" id="fileNameLabel3"><b>Drag File Import disini</b> <br> atau klik area ini dan pilih file</label><br><br>
                        <span id="fileSizeLabel" style="display: none;"></span>
                        <button type="button" class="btn btn-danger btn-sm mt-2" id="deleteFile" style="display: none;">Hapus</button>
                      </div>
                      @if ($errors->has('file'))
                        <span class="text-danger">{{ $errors->first('file') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
              </div>
              <label class="text-start mt-2 mb-2 fw-bold" style="color: #E77575;">Pastikan header tabel tidak dirubah, format penulisan disesuaikan dengan template</label>
            </div>
          </div>
          <div class="modal-footer">
            <div class="col-12">
              <div class="row">
                <div class="col-6"><button type="button" class="btn btn-outline-purple w-100" data-bs-dismiss="modal">Batal</button></div>
                <div class="col-6"><button type="submit" class="btn btn-purple w-100">Import</button></div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</main>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
  document.addEventListener("DOMContentLoaded", function () {
    const fileInput = document.getElementById('file3');
  const deleteFile = document.getElementById('deleteFile');
  const logoImage = document.querySelector('.navbar-brand');
  const fileSizeLabel = document.getElementById('fileSizeLabel');
  
  fileInput.addEventListener('change', () => {
    displayFileName(fileInput);
  });

  const dragFileArea2 = document.getElementById('dragFileArea2');

  dragFileArea2.addEventListener('dragover', function (event) {
    event.preventDefault();
    event.target.classList.add('drag-over');
  });

  dragFileArea2.addEventListener('dragleave', function (event) {
    event.preventDefault();
    event.target.classList.remove('drag-over');
  });

  dragFileArea2.addEventListener('drop', function (event) {
    event.preventDefault();
    event.target.classList.remove('drag-over');

    fileInput.files = event.dataTransfer.files;
    displayFileName(fileInput);
    logoImage.style.display = 'none';
    displayFileSize(fileInput.files[0]);
  });

  function openFilePicker() {
    fileInput.click();
  }

  function formatSizeUnits(bytes) {
    if (bytes >= 1073741824) {
      return (bytes / 1073741824).toFixed(2) + ' GB';
    } else if (bytes >= 1048576) {
      return (bytes / 1048576).toFixed(2) + ' MB';
    } else if (bytes >= 1024) {
      return (bytes / 1024).toFixed(2) + ' KB';
    } else if (bytes > 1) {
      return bytes + ' bytes';
    } else if (bytes === 1) {
      return bytes + ' byte';
    } else {
      return '0 bytes';
    }
  }
      
  function displayFileName(input) {
  const fileNameLabel = document.getElementById('fileNameLabel3');
  const fileError = document.getElementById('fileError');

  if (fileNameLabel) {
    if (input.files && input.files.length > 0) {
      const file = input.files[0];
      fileNameLabel.innerHTML = ` ${file.name}<br><small> ${formatSizeUnits(file.size)}</small>`;
      deleteFile.style.display = 'inline-block';
      if (fileError) {
        fileError.textContent = '';
      }
      displayFileSize(file);
    } else {
      fileNameLabel.innerHTML = '<b>Drag File Import Disini<br>atau klik area ini dan pilih file</b>';
      deleteFile.style.display = 'none';
      if (fileError) {
        fileError.textContent = ''; // Hapus pesan kesalahan jika elemen 'fileError' ada
      }
    }
  } else {
    console.error("Element with ID 'fileNameLabel3' not found.");
  }
  }


  function displayFileSize(file) {
    if (file) {
      // fileSizeLabel.textContent = `Ukuran File: ${formatSizeUnits(file.size)}`;
      fileSizeLabel.style.display = 'block';
    } else {
      fileSizeLabel.style.display = 'none';
    }
  }

  deleteFile.addEventListener('click', () => {
    fileInput.value = null;
    deleteFile.style.display = 'none';
    const fileNameLabel = document.getElementById('fileNameLabel3');
    fileNameLabel.innerHTML = '<b>Drag File Import Disini<br>atau klik area ini dan pilih file</b>';
    logoImage.style.display = 'block';
    fileSizeLabel.style.display = 'none';
  });
  });
</script>

<script>
  function confirmDelete(id, barangName) {
    var confirmationMessage = 'Apakah anda yakin ingin menghapus data "' + barangName + '"?';
    var userConfirmed = window.confirm(confirmationMessage);

    if (userConfirmed) {
      // If user confirmed, submit the form
      var form = document.querySelector('form[action="' + '{{ url('barang/') }}' + id + '"]');
      form.submit();
    }
  }
</script>

@endsection
