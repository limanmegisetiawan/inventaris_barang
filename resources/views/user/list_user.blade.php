@extends('layouts.main')
@section('content')
<main id="main" class="main">
  <section class="section">
    <div class="row">
      <div class="card">
        <div class="text-start mt-4 mb-4 fw-bold">
          <i class="fa-solid fa-user text-purple"></i>&nbsp;
          <span class="fw-bold text-muted mr-4">User Account</span>
        </div>
        <div class="container">
          <div class="row g-2">
            <div class="col-7">
              <div class="border rounded bg-light">
                <form action="/user" class="search-form d-flex align-items-center">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Cari Nama User / username / role" name="search" value="{{ request('search') }}">
                    <button type="submit" class="btn text-start" style="border: none !important;">
                      <i class="bi bi-search"></i>
                    </button>
                  </div>
                </form>
              </div>
            </div>
            <div class="col-1 col-sm-auto">
              <div class="border rounded bg-light">
                <div class="btn-group" >
                  <form action="/user/filter" method="GET" id="filterForm" >
                  <select class="form-select" name="status" id="status">
                    <option selected disabled value >Filter</option>
                      <option value="1" {{ request('status') == '1' ? 'selected' : '' }}>Aktif</option>
                      <option value="0" {{ request('status') == '0' ? 'selected' : '' }}>Tidak Aktif</option>
                  </select>
                  </form>
                </div>
              </div>
            </div>
              <div class="col-1 col-sm-auto">
              <div class="border rounded bg-light">
                <button type="button" class="btn btn-info w-100" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
                  Import
                </button>
              </div>
            </div>
            <div class="col-1 col-sm-auto">
              <div class="border rounded bg-light">
                <a href="/user/exportexcel" target="_blank">
                  <button type="button" class="btn btn-success w-100">Export</button>
                </a>
              </div>
            </div>
            <div class="col-1 col-sm-auto">
              <div class="border rounded bg-light">
                <button type="button" class="btn btn-purple w-100" data-bs-toggle="modal" data-bs-target="#createUser">
                  Tambah User
                </button>
              </div>
            </div>
          </div>
        </div>
        <div class="container mt-4">
          <div class="row">
            @if (session()->has('success'))
              <br>
                <div class="alert alert-success" role="alert">
                  {{ session('success') }}
                </div>
              <br>
            @endif
            @if ($errors->any())
              <br>
                <div class="alert alert-danger" role="alert">
                  @foreach ($errors->all() as $error)
                  {{ $error }}
                  @endforeach
                </div>
              <br>
            @endif  
            @if (Session::has('import_errors'))
              <br>
                @foreach (Session::get('import_errors') as $failure)
                <div class="alert alert-danger" role="alert">
                  {{ $failure->errors()[0] }} at line no-{{ $failure->row() }}
                </div>
                @endforeach
              <br>
            @endif
            <div class="table-responsive">
                <table class="table table-hover table-sm align-middle nowrap">
                  <thead class="table-dark">
                    <tr>
                      <th class="text-center" style="background-color: #355B74; color: #fff;">No</th>
                      <th class="text-center" style="background-color: #355B74; color: #fff;">Nama Lengkap</th>
                      <th class="text-center" style="background-color: #355B74; color: #fff;">Username</th>
                      <th class="text-center" style="background-color: #355B74; color: #fff;">Role</th>
                      <th class="text-center" style="background-color: #355B74; color: #fff;">Status</th>
                      <th class="text-center col-2" style="background-color: #355B74; color: #fff;">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($user as $i => $u)
                      <tr>
                        <td class="text-center">{{ $i + 1 }}</td>
                        <td class="text-center">{{ $u->nama_lengkap }}</td>
                        <td class="text-center">{{ $u->username }}</td>
                        <td class="text-center">{{ $u->role }}</td>
                        <td class="text-center">
                          @if ($u->status == 1) <span class="badge badge-success text-bg-success fw-bold">Aktif</span>
                          @elseif ($u->status == 0) <span class="badge badge-warning text-bg-danger fw-bold">Tidak Aktif</span>
                          @endif
                        </td>
                        <td>
                          <div class="text-center">
                            <form action="{{ url('user/'.$u->id) }}" method="POST">
                              @csrf 
                              @if($u->role !== 'Superadmin')
                                    <button type="button" class="btn btn-sm btn-warning" data-bs-toggle="modal" data-bs-target="#editUser{{ $u->id }}">
                                        <i class="bi bi-pencil-square"></i>
                                    </button>
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button class="btn btn-sm btn-danger" type="button" onclick="confirmDelete('{{ $u->id }}', '{{ $u->username }}')">
                                        <i class="bi bi-trash"></i>
                                    </button>
                              @endif
                            </form>
                          </div>
                          <div class="modal fade editUser" id="editUser{{ $u->id }}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title">Edit User</h5>
                                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                  @include('user.edit_user')
                                </div>
                              </div>
                            </div>
                          </div>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                </table>
                    {{ $user->links() }}
              </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Import Users</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <form action="/user/importexcel" method="POST" enctype="multipart/form-data">
          @csrf
          <div class="modal-body">
            <div class="form-group">
              <label class="text-start mt-2 mb-2 fw-bold">Upload template excel pada form dibawah ini</label>
              <label class="text-start mt-2 mb-2 fw-bold">Sebelum import pastikan download template import dibawah ini terlebih dahulu</label>
              <a href="{{asset('Import/ImportUser.xlsx') }}" class="btn btn-purple w-100" style="margin-bottom: 20px;">Download Template Import</a>
              <div class="row">
                <div style="margin: auto; width: 100%; text-align: center;">
                  <div style="background-color: #f5e1f7; border-radius: 10px; border: 3px dashed rgba(226, 10, 247, 0.65);">
                    <div style="background-color: rgb(245, 225, 247); border-radius: 10px; padding: 10px;">
                      <div class="drag-file" id="dragFileArea1">
                        <img class="navbar-brand" src="/images/frame.png" alt="Logo"><br>
                        <input type="file" name="file" class="form-control-file" id="file4" accept="file/*" required value="{{ old('file') }}" style="visibility: hidden;"><br>
                        <label class="drag-file-text hiden" for="file4" id="fileNameLabel4" style="cursor: pointer;"><b>Drag File Import Disini</b> <br> atau klik area ini dan pilih file</label><br><br>
                        <span id="fileSize" style="display: none;"></span>
                        <button type="button" class="btn btn-danger btn-sm mt-2" id="deleteFileButton" style="display: none;">Hapus</button>
                      </div>
                      @if ($errors->has('file'))
                        <span class="text-danger">{{ $errors->first('file') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
              </div>
              <label class="text-start mt-2 mb-2 fw-bold" style="color: #E77575;">Pastikan header tabel tidak dirubah, format penulisan disesuaikan dengan template</label>
            </div>
            <div class="col-12 mt-3">
              <div class="row">
                <div class="col-6">
                  <a href="/user">
                    <button type="button" class="btn btn-outline-purple w-100">Batal</button>
                  </a>
                </div>
                <div class="col-6"><button type="submit" class="btn btn-purple w-100">Import</button></div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

  <div class="modal fade" id="createUser" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="createUserLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="createUserLabel">Tambah User</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
              @if ($errors->any())
              <br>
                <div class="alert alert-danger" role="alert">
                  @foreach ($errors->all() as $error)
                  {{ $error }}
                  @endforeach
                </div>
              <br>
              @endif
          @include('User.create_user')
        </div>
      </div>
    </div>
  </div>
</main>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

<script>
  const dropdown = document.getElementById('status');
  const filterForm = document.getElementById('filterForm');

  dropdown.addEventListener('change', function() {
    if (dropdown.value === '1' || dropdown.value === '0') {
      filterForm.submit();
    }
  });
</script>

<script>
  $(document).ready(function() {
    $('button[data-toggle="modal"]').click(function() {
      var targetModal = $(this).data('target');
      $(targetModal).modal('show');
    });

    $('.modal').on('hidden.bs.modal', function() {
      $(this).find('form')[0].reset();
    });

    $('#filter-form').submit(function(e) {
      e.preventDefault();

      var status = $('#status').val();
      $.ajax({
        url: $(this).attr('action'),
        type: 'GET',
        data: { status: status },
        success: function(response) {
          $('#user-table').html(response);
        },
        error: function(xhr, status, error) {
          console.error(error);
        }
      });
    });

    $('.btn-active-edit-user').hide();
    $('.btn-active-edit-user').on('change', function() {
      var id = $(this).attr('data-id');
      $('#label-edit-input-not-active'+ id + ', #label-edit-input-active' + id).addClass("btn-outline-purple");
      if ($(this).val() == 1) {
        $("#edit-input-active" + id).prop("checked", true);
        $('#label-edit-input-active' + id).removeClass("btn-outline-purple");
        $('#label-edit-input-active' + id).addClass("btn-purple");
        $('#label-edit-input-not-active' + id).removeClass("btn-purple");
      } else {
        $("#edit-input-not-active" + id).prop("checked", true);
        $('#label-edit-input-not-active' + id).removeClass("btn-outline-purple");
        $('#label-edit-input-not-active' + id).addClass("btn-purple");
        $('#label-edit-input-active' + id).removeClass("btn-purple");
      }
    });

  });
  
  function percentageToDegrees(percentage) {
    return percentage / 100 * 360
  }
</script>


<script>
  document.addEventListener("DOMContentLoaded", function () {
    const fileInput = document.getElementById('file4');
  const deleteFileButton = document.getElementById('deleteFileButton');
  const logoImage = document.querySelector('.navbar-brand');
  const fileSize = document.getElementById('fileSize');
  
  fileInput.addEventListener('change', () => {
    displayFileName(fileInput);
  });

  const dragFileArea1 = document.getElementById('dragFileArea1');

  dragFileArea1.addEventListener('dragover', function (event) {
    event.preventDefault();
    event.target.classList.add('drag-over');
  });

  dragFileArea1.addEventListener('dragleave', function (event) {
    event.preventDefault();
    event.target.classList.remove('drag-over');
  });

  dragFileArea1.addEventListener('drop', function (event) {
    event.preventDefault();
    event.target.classList.remove('drag-over');

    fileInput.files = event.dataTransfer.files;
    displayFileName(fileInput);
    logoImage.style.display = 'none';
    displayFileSize(fileInput.files[0]);
  });

  function openFilePicker() {
    fileInput.click();
  }

  function formatSizeUnits(bytes) {
    if (bytes >= 1073741824) {
      return (bytes / 1073741824).toFixed(2) + ' GB';
    } else if (bytes >= 1048576) {
      return (bytes / 1048576).toFixed(2) + ' MB';
    } else if (bytes >= 1024) {
      return (bytes / 1024).toFixed(2) + ' KB';
    } else if (bytes > 1) {
      return bytes + ' bytes';
    } else if (bytes === 1) {
      return bytes + ' byte';
    } else {
      return '0 bytes';
    }
  }
      
  function displayFileName(input) {
  const fileNameLabel = document.getElementById('fileNameLabel4');
  const fileError = document.getElementById('fileError');

  if (fileNameLabel) {
    if (input.files && input.files.length > 0) {
      const file = input.files[0];
      fileNameLabel.innerHTML = ` ${file.name}<br><small> ${formatSizeUnits(file.size)}</small>`;
      deleteFileButton.style.display = 'inline-block';
      if (fileError) {
        fileError.textContent = '';
      }
      displayFileSize(file);
    } else {
      fileNameLabel.innerHTML = '<b>Drag File Import Disini<br>atau klik area ini dan pilih file</b>';
      deleteFileButton.style.display = 'none';
      if (fileError) {
        fileError.textContent = ''; // Hapus pesan kesalahan jika elemen 'fileError' ada
      }
    }
  } else {
    console.error("Element with ID 'fileNameLabel4' not found.");
  }
  }


  function displayFileSize(file) {
    if (file) {
      // fileSize.textContent = `Ukuran File: ${formatSizeUnits(file.size)}`;
      fileSize.style.display = 'block';
    } else {
      fileSize.style.display = 'none';
    }
  }

  deleteFileButton.addEventListener('click', () => {
    fileInput.value = null;
    deleteFileButton.style.display = 'none';
    const fileNameLabel = document.getElementById('fileNameLabel4');
    fileNameLabel.innerHTML = '<b>Drag File Import Disini<br>atau klik area ini dan pilih file</b>';
    logoImage.style.display = 'block';
    fileSize.style.display = 'none';
  });
  });
</script>

<script>
  function confirmDelete(id, barangName) {
    var confirmationMessage = 'Apakah anda yakin ingin menghapus data "' + barangName + '"?';
    var userConfirmed = window.confirm(confirmationMessage);

    if (userConfirmed) {
      // If user confirmed, submit the form
      var form = document.querySelector('form[action="' + '{{ url('user/') }}' + id + '"]');
      form.submit();
    }
  }
</script>

@endsection