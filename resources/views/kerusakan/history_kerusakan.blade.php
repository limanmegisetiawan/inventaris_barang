@php $data = array(); @endphp
@foreach ($k->activities->reverse() as $index => $a)
    @php
    $barangId = json_decode($a->properties)->attributes->barang_id;
    $barang = $barang->firstWhere('id', $barangId);
    $status = json_decode($a->properties)->attributes->status;

    // Cek status dan masukkan data ke array $data sesuai urutan status
    if ($status === 'Diterima') {
        $data[0] = array(
            'barang' => $barang->nama_barang,
            'kerusakan_barang' => json_decode($a->properties)->attributes->kerusakan_barang,
            'catatan_service' => json_decode($a->properties)->attributes->catatan_service,
            'catatan_selesai' => json_decode($a->properties)->attributes->catatan_selesai,
            'catatan_serahkan' => json_decode($a->properties)->attributes->catatan_serahkan,
            'nama_penerima' => json_decode($a->properties)->attributes->nama_penerima,
            'status' => $status,
            'datetime' => $a->created_at
        );
    } elseif ($status === 'Service') {
        $data[1] = array(
            'barang' => $barang->nama_barang,
            'kerusakan_barang' => json_decode($a->properties)->attributes->kerusakan_barang,
            'catatan_service' => json_decode($a->properties)->attributes->catatan_service,
            'catatan_selesai' => json_decode($a->properties)->attributes->catatan_selesai,
            'catatan_serahkan' => json_decode($a->properties)->attributes->catatan_serahkan,
            'nama_penerima' => json_decode($a->properties)->attributes->nama_penerima,
            'status' => $status,
            'datetime' => $a->created_at
        );
    } elseif ($status === 'Selesai') {
        $data[2] = array(
            'barang' => $barang->nama_barang,
            'kerusakan_barang' => json_decode($a->properties)->attributes->kerusakan_barang,
            'catatan_service' => json_decode($a->properties)->attributes->catatan_service,
            'catatan_selesai' => json_decode($a->properties)->attributes->catatan_selesai,
            'catatan_serahkan' => json_decode($a->properties)->attributes->catatan_serahkan,
            'nama_penerima' => json_decode($a->properties)->attributes->nama_penerima,
            'status' => $status,
            'datetime' => $a->created_at
        );
    } elseif ($status === 'Diserahkan') {
        $data[3] = array(
            'barang' => $barang->nama_barang,
            'kerusakan_barang' => json_decode($a->properties)->attributes->kerusakan_barang,
            'catatan_service' => json_decode($a->properties)->attributes->catatan_service,
            'catatan_selesai' => json_decode($a->properties)->attributes->catatan_selesai,
            'catatan_serahkan' => json_decode($a->properties)->attributes->catatan_serahkan,
            'nama_penerima' => json_decode($a->properties)->attributes->nama_penerima,
            'status' => $status,
            'datetime' => $a->created_at
        );
    }elseif ($status === 'Ditutup') {
        $data[4] = array(
            'status' => $status,
            'datetime' => $a->created_at
        );
    }
    @endphp
@endforeach

<ul class="history-tracking">
    @if (!empty($data[4]))
    <li>
        <p>
            <div class="row">
                <div class="mr-2">
                    Laporan Kerusakan Ditutup
                </div>
                <div class="mr-2">
                    <br><span class="text-muted">{{ date('d F Y H:i', strtotime($data[2]['datetime'])) }}</span>
                </div>
            </div>
        </p>
    </li>
@endif
    @if (!empty($data[3]))
        <li>
            <p>
                Barang <span class="fw-bold">{{ $data[3]['barang'] }}</span> sudah diserahkan kepada
                <span class="fw-bold">{{ $k->penerima_barang }}</span>
                <br>
                Catatan : <span class="text-muted">{{ $k->catatan_serahkan }}</span>
                <br><span class="text-muted">{{ date('d F Y H:i', strtotime($data[3]['datetime'])) }}</span>
            </p>
        </li>
    @endif
    @if (!empty($data[2]))
        <li>
            <p>
                Barang <span class="fw-bold">{{ $data[2]['barang'] }}</span> sudah selesai diservice
                <br>
                Catatan : <span class="text-muted">{{ $k->catatan_selesai }}</span>
                <br><span class="text-muted">{{ date('d F Y H:i', strtotime($data[2]['datetime'])) }}</span>
            </p>
        </li>
    @endif
    @if (!empty($data[1]))
        <li>
            <p>
                Barang <span class="fw-bold">{{ $data[1]['barang'] }}</span> sedang diservice
                <br>
                Oleh
                <span class="fw-bold">{{ $k->nama_penyervice }}</span>
                <br>
                Catatan : <span class="text-muted">{{ $k->catatan_service }}</span>
                <br><span class="text-muted">{{ date('d F Y H:i', strtotime($data[1]['datetime'])) }}</span>
            </p>
        </li>
    @endif
    @if (!empty($data[0]))
        <li>
            <p>
                Barang <span class="fw-bold">{{ $data[0]['barang'] }}</span> telah diterima 
                <br>
                Oleh
                <span class="fw-bold">{{ $k->nama_penerima }}</span>
                <br>
                kerusakan : <span class="text-muted">{{ $k->kerusakan_barang }}</span>
                <br><span class="text-muted">{{ date('d F Y H:i', strtotime($data[0]['datetime'])) }}</span>
            </p>
        </li>
    @endif
</ul>
