@extends('layouts.main')
@section('content')
<main id="main" class="main">
  <section class="section">
    <div class="row">
      <div class="card">
        <div class="text-start mt-4 mb-4 fw-bold">
          <i class="fa-solid fa-triangle-exclamation text-purple"></i>&nbsp;
          <span class="fw-bold text-muted mr-4">Kerusakan Barang</span>
        </div>
        <div class="container">
          <div class="row">
            <div class="col-sm-6 col-lg-6">
              <div class="card text-white bg-flat-color-1">
                <div class="card-body pb-0">
                  <div class="row">
                    <div class="col-lg-2">
                      <div class="text-black fw-bold d-flex align-items-center" style="background-color: #EEF4FF; padding: 5px; margin-bottom: 5px; width: fit-content;">
                        <i class="bi bi-box" style="font-size: 2.5rem; margin-right: 8px;"></i>
                      </div>
                    </div>
                    <div class="col-lg-8">
                      <div class="row">
                        <div class="col">
                          <div class="text-black fw-bold">
                            <span style="font-size: 20px;  margin-left: 8px;">Total kerusakan barang</span>
                          </div>
                          <div class="text-black fw-bold">
                            <span style="font-size: 20px;  margin-left: 8px;"> {{ $totalKerusakan }}</span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-lg-6">
              <div class="card text-white bg-flat-color-1">
                <div class="card-body pb-0">
                  <div class="row">
                    <div class="col-lg-2">
                      <div class="text-black fw-bold d-flex align-items-center" style="background-color: #EEF4FF; padding: 5px; margin-bottom: 5px; width: fit-content;">
                        <i class="bi bi-box" style="font-size: 2.5rem; margin-right: 8px;"></i>
                      </div>
                    </div>
                    <div class="col-lg-8">
                      <div class="row">
                        <div class="col">
                          <div class="text-black fw-bold">
                            <span style="font-size: 20px;  margin-left: 8px;">Total barang yang sedang diservice</span>
                          </div>
                          <div class="text-black fw-bold">
                            <span style="font-size: 20px;  margin-left: 8px;"> {{ $totalService }}</span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-lg-6">
              <div class="card text-white bg-flat-color-1">
                <div class="card-body pb-0">
                  <div class="row">
                    <div class="col-lg-2">
                      <div class="text-black fw-bold d-flex align-items-center" style="background-color: #EEF4FF; padding: 5px; margin-bottom: 5px; width: fit-content;">
                        <i class="bi bi-box" style="font-size: 2.5rem; margin-right: 8px;"></i>
                      </div>
                    </div>
                    <div class="col-lg-8">
                      <div class="row">
                        <div class="col">
                          <div class="text-black fw-bold">
                            <span style="font-size: 20px;  margin-left: 8px;">Total barang yang telah selesai diservice</span>
                          </div>
                          <div class="text-black fw-bold">
                            <span style="font-size: 20px;  margin-left: 8px;"> {{ $totalSelesai }}</span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-lg-6">
              <div class="card text-white bg-flat-color-1">
                <div class="card-body pb-0">
                  <div class="row">
                    <div class="col-lg-2">
                      <div class="text-black fw-bold d-flex align-items-center" style="background-color: #EEF4FF; padding: 5px; margin-bottom: 5px; width: fit-content;">
                        <i class="bi bi-box" style="font-size: 2.5rem; margin-right: 8px;"></i>
                      </div>
                    </div>
                    <div class="col-lg-8">
                      <div class="row">
                        <div class="col">
                          <div class="text-black fw-bold">
                            <span style="font-size: 20px; margin-left: 8px;">Total barang diserahkan ke penanggung jawab</span>
                          </div>
                          <div class="text-black fw-bold">
                            <span style="font-size: 20px; margin-left: 8px;">{{ $totalDiserahkan }}</span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="container">
          <div class="row g-2">
            <div class="col-7">
              <div class="border rounded bg-light">
                <form action="/kerusakan" class="search-form d-flex align-items-center">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Cari Nama Barang / nama pemilik" name="search" value="{{ request('search') }}">
                    <button type="submit" class="btn text-start" style="border: none !important;">
                      <i class="bi bi-search"></i>
                    </button>
                  </div>
                </form>
              </div>
            </div>
            <div class="col-1 col-sm-auto">
              <div class="border rounded bg-light">
                <div class="btn-group" >
                  <form action="/kerusakan/filter" method="GET" id="filterForm" >
                  <select class="form-select" name="status" id="status" >
                    <option selected disabled value >Filter</option>
                    <option value="Diterima" {{ request('status') == 'Diterima' ? 'selected' : '' }}>Diterima</option>
                    <option value="Service" {{ request('status') == 'Service' ? 'selected' : '' }}>Service</option>
                    <option value="Selesai" {{ request('status') == 'Selesai' ? 'selected' : '' }}>Selesai</option>
                    <option value="Diserahkan" {{ request('status') == 'Diserahkan' ? 'selected' : '' }}>Diserahkan</option>
                    <option value="Ditutup" {{ request('status') == 'Ditutup' ? 'selected' : '' }}>Ditutup</option>
                  </select>
                  </form>
                </div>
              </div>
            </div>
            <div class="col-1 col-sm-auto">
              <div class="border rounded bg-light">
                <button type="button" class="btn btn-success w-100 " data-bs-toggle="modal" data-bs-target="#exportKerusakan">
                  Export
                </button>
              </div>
            </div>
            <div class="col-1 col-sm-auto">
              <div class="border rounded bg-light">
                <a href="/kerusakan/create">
                  <button type="button" class="btn btn-purple w-100">Tambah Kerusakan</button>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div class="container mt-4">
          <div class="row">
             @if (session()->has('success'))
              <br>
                <div class="alert alert-success" role="alert">
                  {{ session('success') }}
                </div>
              <br>
              @endif
              @if ($errors->any())
              <br>
                <div class="alert alert-danger" role="alert">
                  @foreach ($errors->all() as $error)
                  {{ $error }}
                  @endforeach
                </div>
              <br>
              @endif
              <table class="table table-hover table-sm align-middle nowrap">
                <thead class="table-dark">
                  <tr>
                    <th class="text-center" style="background-color: #355B74; color: #fff;">No</th>
                    <th class="text-center" style="background-color: #355B74; color: #fff;">Tanggal</th>
                    <th class="text-center" style="background-color: #355B74; color: #fff;">Nama Barang</th>
                    <th class="text-center" style="background-color: #355B74; color: #fff;">Nama Penanggung Jawab</th>
                    <th class="text-center" style="background-color: #355B74; color: #fff;">Nama Jasa Service</th>
                    <th class="text-center" style="background-color: #355B74; color: #fff;">Kerusakan</th>
                    <th class="text-center" style="background-color: #355B74; color: #fff;">Status</th>
                    <th class="text-center col-2" style="background-color: #355B74; color: #fff;">Action</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($kerusakan as $i => $k)
                    <tr>
                      <td class="text-center">{{ $i + 1 }}</td>
                      <td class="text-center">{{ $k->updated_at->format('d F Y') }}</td>
                      <td class="text-center">{{ $k->barang->nama_barang }}</td>
                      <td class="text-center">{{ $k->barang->nama_pemilik }}</td>
                      <td class="text-center">{{ $k->nama_penyervice }}</td>
                      <td class="text-center">{{ $k->kerusakan_barang }}</td>
                      <td class="text-center">
                        @if ($k->status == "Selesai") <span class="badge badge-success text-bg-success fw-bold">Selesai</span>
                        @elseif ($k->status == "Service") <span class="badge badge-warning text-bg-danger fw-bold">Service</span>
                        @elseif ($k->status == "Diterima") <span class="badge badge-primary text-bg-primary fw-bold">Diterima</span>
                        @elseif ($k->status == "Diserahkan") <span class="badge badge-success text-bg-success fw-bold">Diserahkan</span>
                        @elseif ($k->status == "Ditutup") <span class="badge badge-success text-bg-info fw-bold">Ditutup</span>
                        @endif
                      </td>
                      <td>
                        <div class="text-center">
                          <button type="button" class="btn btn-sm btn-warning" data-bs-toggle="modal" data-bs-target="#editModal{{ $k->id }}">
                            <i class="fa-solid fa-arrows-rotate" style="color: white"></i>
                          </button>
                          <button type="button" class="btn btn-sm btn-purple" data-bs-toggle="modal" data-bs-target="#history{{ $k->id }}">
                            <i class="fa-solid fa-clock-rotate-left" style="color: white"></i>
                          </button>
                          <div class="btn-group">
                            <button class="btn btn-sm" type="button" id="dropdownMenu2" data-bs-toggle="dropdown" aria-expanded="false">
                              <i class="fa-solid fa-ellipsis-vertical"></i>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
                              <li>
                                <button class="dropdown-item" type="button" data-bs-toggle="modal" data-bs-target="#editKerusakan{{ $k->id }}">Edit</button>
                              </li>
                              <li>
                                <form action="{{ url('kerusakan/'.$k->id) }}" method="POST">
                                  @csrf 
                                  <input type="hidden" name="_method" value="DELETE">
                                  <button class="dropdown-item"  type="submit" onclick="confirmDelete('{{ $k->id }}', '{{ $k->barang->nama_barang }}')">Hapus</button>
                                </form>
                              </li>
                            </ul>
                          </div>
                        </div>
                        <div class="modal fade" id="editModal{{ $k->id }}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="editModal{{ $k->id }}Label" aria-hidden="true">
                          <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title">Update Status Data Service</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                              </div>
                              <div class="modal-body">
                                <div class="alert alert-info d-flex flex-row" role="alert">
                                  <div class="row">
                                    <div class="col-auto align-self-center">
                                      <i class="fa-solid fa-circle-info fa-2xl"></i>
                                    </div>
                                    <div class="col">
                                      <h >Update Status secara berkala dari mulai diterima hinggal barang telah diambil</h>
                                    </div>
                                  </div>
                                </div>
                                <form action="/kerusakan/{{ $k->id }}" method="POST">
                                  @csrf
                                  @method('PUT')
                                  <div class="form-group mb-3">
                                    <label class="text-start">Status </label><small class="text-danger"> *</small>
                                    <div class="alert alert-secondary" role="alert">
                                      <div class="container">
                                        <div class="row input-group text-center">
                                          <div class="col">
                                            {{-- <input type="radio" name="status" class="btn-check btn-diterima" data-id="{{ $k->id }}" id="Service{{ $k->id }}" value="Service" {{ old('status', $k->status) == 'Service' ? 'checked' : '' }}> --}}
                                            <input type="radio" name="status" class="btn-check" ids="{{ $k->id }}" id="Service{{ $k->id }}" value="Service" {{ old('status', $k->status) == 'Service' ? 'checked' : '' }}>
                                            <label class="btn btn-outline-purple w-100" for="Service{{ $k->id }}">Service</label>
                                          </div>
                                          <div class="col">
                                            <input type="radio" name="status" class="btn-check" ids="{{ $k->id }}" id="Selesai{{ $k->id }}" value="Selesai" {{ old('status', $k->status) == 'Selesai' ? 'checked' : '' }}>
                                            <label class="btn btn-outline-purple w-100" for="Selesai{{ $k->id }}">Selesai</label>
                                          </div>
                                          <div class="col">
                                            <input type="radio" name="status" class="btn-check" ids="{{ $k->id }}" id="Diserahkan{{ $k->id }}" value="Diserahkan" {{ old('status', $k->status) == 'Diserahkan' ? 'checked' : '' }}>
                                            <label class="btn btn-outline-purple w-100" for="Diserahkan{{ $k->id }}">Diserahkan</label>
                                          </div>
                                          <div class="col">
                                            <input type="radio" name="status" class="btn-check" ids="{{ $k->id }}" id="Ditutup{{ $k->id }}" value="Ditutup" {{ old('status', $k->status) == 'Ditutup' ? 'checked' : '' }}>
                                            <label class="btn btn-outline-purple w-100" for="Ditutup{{ $k->id }}">Tutup</label>
                                          </div>
                                        </div>
                                        @error('status')
                                        <div class="invalid-feedback">
                                          {{ $message }}
                                        </div>
                                        @enderror
                                      </div>
                                    </div>
                                  </div>
                                  <input type="hidden" id="statusAwal{{ $k->id }}" value="{{ $k->status }}">
                                  <div id="input1{{ $k->id }}"  style="display: none;">
                                    <div class="form-group">
                                      <label class="text-start">Di Service Oleh </label>
                                      <input type="text" name="nama_penyervice" class="form-control @error('nama_penyervice') is-invalid @enderror" id="nama_penyervice{{ $k->id }}" placeholder="Masukan nama tempat service"   value="{{  $k->nama_penyervice }}" >
                                      @error('nama_penyervice')
                                      <div class="invalid-feedback">
                                        {{ $message }}
                                      </div>  
                                      @enderror
                                    </div>
                                    <div class="form-group">
                                      <label class="text-start">Catatan (Optional)</label>
                                      <textarea class="form-control @error('catatan_service') is-invalid @enderror" id="catatan_service{{ $k->id }}" rows="4" cols="50" placeholder="Masukan catatan apabila ada catatan" name="catatan_service" >{{ old('catatan_service', $k->catatan_service) }}</textarea>
                                      @error('catatan_service')
                                        <div class="invalid-feedback">
                                          {{ $message }}
                                        </div>
                                      @enderror
                                    </div>
                                  </div>
                                  <div id="input2{{ $k->id }}" class="form-group" style="display: none;">
                                    <label class="text-start">Catatan (Optional)</label>
                                      <textarea class="form-control @error('catatan_selesai') is-invalid @enderror" id="catatan_selesai{{ $k->id }}" rows="4" cols="50" placeholder="Masukan catatan apabila ada catatan" name="catatan_selesai"  >{{ old('catatan_selesai', $k->catatan_selesai) }}</textarea>
                                      @error('catatan_selesai')
                                        <div class="invalid-feedback">
                                          {{ $message }}
                                        </div>
                                      @enderror
                                  </div>
                                  <div id="input3{{ $k->id }}" style="display: none;">
                                    <div class="form-group">
                                      <label class="text-start">Diserahkan Kepada </label><small class="text-danger"> *</small>
                                      <input type="text" name="penerima_barang" class="form-control @error('penerima_barang') is-invalid @enderror" id="penerima_barang{{ $k->id }}" placeholder="Masukan nama pemilik barang"   value="{{  $k->penerima_barang }}">
                                      @error('penerima_barang')
                                      <div class="invalid-feedback">
                                        {{ $message }}
                                      </div>  
                                      @enderror
                                    </div>
                                    <div class="form-group">
                                      <label class="text-start">Catatan (Optional)</label>
                                      <textarea class="form-control @error('catatan_serahkan') is-invalid @enderror" id="catatan_serahkan{{ $k->id }}" rows="4" cols="50" placeholder="Masukan catatan apabila ada catatan" name="catatan_serahkan" >{{ old('catatan_serahkan', $k->catatan_serahkan) }}</textarea>
                                      @error('catatan_serahkan')
                                        <div class="invalid-feedback">
                                          {{ $message }}
                                        </div>
                                      @enderror
                                    </div>
                                  </div>
                                  <div class="col-12 mt-3">
                                    <div class="row">
                                      <div class="col-6">
                                        <a href="/kerusakan">
                                          <button type="button" class="btn btn-outline-purple w-100">Batal</button>
                                        </a>
                                      </div>
                                      <div class="col-6"><button type="submit" class="btn btn-purple w-100">Simpan</button></div>
                                    </div>
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="modal fade" id="history{{ $k->id }}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"  role="dialog" aria-labelledby="history{{ $k->id }}Label" aria-hidden="true">
                          <div class="modal-dialog modal-dialog-scrollable">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="historyModal{{ $k->id }}Label">History Data Service</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                              </div>
                              <div class="modal-body">
                                @include('kerusakan.history_kerusakan', ['activities' => $activities, 'activityId' => $k->id])
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="modal fade" id="editKerusakan{{ $k->id }}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="editKerusakan{{ $k->id }}Label" aria-hidden="true">
                          <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="editKerusakan{{ $k->id }}Label">Edit Kerusakan Barang</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                              </div>
                              <div class="modal-body">
                                @include('kerusakan.edit_kerusakan')
                              </div>
                            </div>
                          </div>
                        </div>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
              {{ $kerusakan->links() }}
          </div>
        </div>
        <div class="modal fade" id="exportKerusakan" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Export Data Kerusakan</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="border rounded bg-light">
                <form action="{{ route('kerusakan.exportData') }}" method="POST">
                  @csrf
                  <div class="modal-body">
                    <div class="form-group">
                      <label class="text-start mt-2 mb-2 fw-bold">Export semua data barang dengan cara klik export semua data dibawah ini</label>
                      <a href="{{ route('kerusakan.exportDataAll') }}" class="btn btn-purple w-100" style="margin-bottom: 20px;">Export Semua Data</a>
                      <div class="row">
                        <label class="text-start mt-2 mb-2 fw-bold">Export data bedasarkan input tanggal yang anda pilih</label>
                        <label class="text-start mt-2 mb-2 fw-bold">Sebelum export pastikan tanggal export dibawah ini terlebih dahulu</label>
                        <div class="col-6">
                          <div class="form-group">
                            <label for="start_date">Tanggal Awal</label>
                            <input type="date" id="start_date" name="start_date" class="form-control" required>
                          </div>
                        </div>
                        <div class="col-6">
                          <div class="form-group">
                            <label for="end_date">Tanggal Akhir</label>
                            <input type="date" id="end_date" name="end_date" class="form-control" required>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <div class="col-12">
                      <div class="row">
                        <div class="col-6"><button type="button" class="btn btn-outline-purple w-100" data-bs-dismiss="modal">Batal</button></div>
                        <div class="col-6"><button type="submit" class="btn btn-purple w-100">Export</button></div>
                      </div>
                    </div>
                  </div>
                </form>
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</main>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

<script>
  $(document).ready(function() {
    function showInput1IfDiterima(selectId) {
      if ($('#statusAwal' + selectId).val() === 'Diterima') {
        $('#input1' + selectId).show();
        $('#Selesai' + selectId).prop('disabled', true);
        $('#Diserahkan' + selectId).prop('disabled', true);
        $('#Service' + selectId).prop('checked', true);
        $('#Ditutup' + selectId).prop('disabled', true);

        enableRequiredInput(selectId, ['nama_penyervice', 'catatan_service']);
      } else {
        disableRequiredInput(selectId, ['nama_penyervice', 'catatan_service']);
      }
    }

    function showInput2IfService(selectId) {
      if ($('#statusAwal' + selectId).val() === 'Service') {
        $('#input2' + selectId).show();
        $('#Service' + selectId).prop('disabled', false);
        $('#Diserahkan' + selectId).prop('disabled', true);
        $('#Selesai' + selectId).prop('checked', true);
        $('#Ditutup' + selectId).prop('disabled', true);
      }
    }

    function showInput3IfDiserahkan(selectId) {
      if ($('#statusAwal' + selectId).val() === 'Selesai') {
        $('#input3' + selectId).show();
        $('#Service' + selectId).prop('disabled', false);
        $('#Selesai' + selectId).prop('disabled', false);
        $('#Diserahkan' + selectId).prop('checked', true);
        $('#Ditutup' + selectId).prop('disabled', true);
        disableRequiredInput(selectId, ['nama_penyervice', 'catatan_service']);
      }
    }

    function disableAllRadioButtonsIfDiserahkan(selectId) {
      if ($('#statusAwal' + selectId).val() === 'Diserahkan') {
        $('#input3' + selectId).show();
        $('#Service' + selectId).prop('disabled', false);
        $('#Selesai' + selectId).prop('disabled', false);
        $('#Diserahkan' + selectId).prop('checked', true);
        $('#Ditutup' + selectId).prop('disabled', false);
        disableRequiredInput(selectId, ['nama_penyervice', 'catatan_service', 'catatan_selesai', 'penerima_barang', 'catatan_serahkan']);
      }
    }

    function disableAllRadioButtonsIfDitutup(selectId) {
      if ($('#statusAwal' + selectId).val() === 'Ditutup') {
        $('.btn-check[ids="' + selectId + '"]').prop('disabled', true);
        disableRequiredInput(selectId, ['nama_penyervice', 'catatan_service', 'catatan_selesai', 'penerima_barang', 'catatan_serahkan']);
      }
    }

    function enableRequiredInput(selectId, fields) {
      for (var i = 0; i < fields.length; i++) {
        $('#' + fields[i] + selectId).prop('required', true);
      }
    }

    function disableRequiredInput(selectId, fields) {
      for (var i = 0; i < fields.length; i++) {
        $('#' + fields[i] + selectId).prop('required', false);
      }
    }

    $('.btn-check').each(function() {
      var selectId = $(this).attr('ids');
      showInput1IfDiterima(selectId);
      showInput2IfService(selectId);
      showInput3IfDiserahkan(selectId);
      disableAllRadioButtonsIfDiserahkan(selectId);
      disableAllRadioButtonsIfDitutup(selectId);
    });

    $('.btn-check').change(function() {
      var selectedOption = $(this).attr('id');
      var selectId = $(this).attr('ids');
      $('[id^="input"]').hide();

      if (selectedOption === 'Service' + selectId) {
        $('#input1' + selectId).show();
      } else if (selectedOption === 'Selesai' + selectId) {
        $('#input2' + selectId).show();
      } else if (selectedOption === 'Diserahkan' + selectId) {
        $('#input3' + selectId).show();
      } else if (selectedOption === 'Ditutup' + selectId) {
      } else {
        $('[id^="input"]').hide();
        $('#Selesai' + selectId).prop('disabled', false);
        $('#Service' + selectId).prop('disabled', false);
        $('#Diserahkan' + selectId).prop('disabled', false);
        disableRequiredInput(selectId, ['nama_penyervice', 'catatan_service', 'catatan_selesai', 'penerima_barang', 'catatan_serahkan']);
      }
    });
  });
</script>

<script>
  const dropdown = document.getElementById('status');
  const filterForm = document.getElementById('filterForm');

  dropdown.addEventListener('change', function() {
    if (dropdown.value === 'Diterima' || dropdown.value === 'Service' || dropdown.value === 'Selesai' || dropdown.value === 'Diserahkan' || dropdown.value === 'Ditutup') {
      filterForm.submit();
    }
  });
</script>

<script>
  function confirmDelete(id, barangName) {
    var confirmationMessage = 'Apakah anda yakin ingin menghapus data "' + barangName + '"?';
    var userConfirmed = window.confirm(confirmationMessage);

    if (userConfirmed) {
      // If user confirmed, submit the form
      var form = document.querySelector('form[action="' + '{{ url('kerusakan/') }}' + id + '"]');
      form.submit();
    }
  }
</script>



@endsection

