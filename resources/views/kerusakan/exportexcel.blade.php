<div class="table-responsive col-lg-8">
    <table class="table table-striped table-sm8">
      <thead>
        <tr>
          <th>No</th>
          <th>Nama Penanggung Jawab</th>
          <th>Nama Barang</th>
          <th>Serial Number</th>
          <th>Kode Barang</th>
          <th>Satuan</th>
          <th>Jumlah</th>
          <th>Tanggal</th>
          <th>Kerusakan Barang</th>
          <th>Status</th>
          <th>nama Penerima Barang Service</th>
          <th>Nama Penyervice</th>
          <th>Catatan Service</th>
          <th>Catatan Selesai</th>
          <th>Catatan Diserahkan</th>
          <th>Nama Penerima Barang</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($kerusakan as $i => $k )
        <tr>
            <td>{{ $i+1 }}</td>
            <td>{{ $k->barang->nama_pemilik ?? ''}}</td>
            <td>{{ $k->barang->nama_barang ?? ''}}</td>
            <td>{{ $k->barang->serial_number ?? ''}}</td>
            <td>{{ $k->barang->kode_barang?? ''}}</td>
            <td>{{ $k->barang->satuan ?? ''}}</td>
            <td>{{ $k->jumlah_rusak }}</td>
            <td>{{ $k->updated_at->format('d F Y H:i') }}</td>
            <td>{{ $k->kerusakan_barang }}</td>
            <td>{{ $k->status }}</td>
            <td>{{ $k->nama_penerima }}</td>
            <td>
              @if ($k->nama_penyervice)
                  {{ $k->nama_penyervice }}
              @else
                  -
              @endif
          </td>
            <td>
              @if (!empty($k->catatan_service))
                  {{ $k->catatan_service }}
              @else
                  -
              @endif

            </td>
          <td>
            @if (!empty($k->catatan_selesai))
                {{ $k->catatan_selesai }}
            @else
                -
            @endif
          </td>
          <td>
            @if ($k->catatan_serahkan)
                {{ $k->catatan_serahkan }}
            @else
                -
            @endif
          </td>
          
        <td>
          @if ($k->penerima_barang)
              {{ $k->penerima_barang }}
          @else
              -
          @endif
      </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>